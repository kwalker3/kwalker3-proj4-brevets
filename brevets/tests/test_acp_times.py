"""
Nose tests acp_times.py

Author: Kayla Walker
"""
from acp_times import open_time, close_time
import nose    # Testing framework
import logging


def test_open_acp_times():
    assert open_time(0, 200, "2019-11-11T00:00-08:00") == "2019-11-11T00:00:00-08:00"
    assert open_time(50, 200, "2019-11-11T00:00-08:00") == "2019-11-11T01:28:00-08:00"
    assert open_time(200, 200, "2019-11-11T00:00-08:00") == "2019-11-11T05:53:00-08:00"


def test_close_before_60km_acp_times():
    assert close_time(0, 200, "2019-11-11T00:00-08:00") == "2019-11-11T01:00:00-08:00"
    assert close_time(50, 200, "2019-11-11T00:00-08:00") == "2019-11-11T03:30:00-08:00"


def test_close_after_60km_acp_times():
    assert close_time(61, 600, "2019-11-11T00:00-08:00") == "2019-11-11T04:04:00-08:00"
    assert close_time(200, 200, "2019-11-11T00:00-08:00") == "2019-11-11T13:30:00-08:00"


def test_close_limit_acp_times():
    assert close_time(220, 200, "2019-11-11T00:00-08:00") == "2019-11-11T13:30:00-08:00"
    assert close_time(360, 300, "2019-11-11T00:00-08:00") == "2019-11-11T20:00:00-08:00"
    assert close_time(440, 400, "2019-11-11T00:00-08:00") == "2019-11-12T03:00:00-08:00"
    assert close_time(615, 600, "2019-11-11T00:00-08:00") == "2019-11-12T16:00:00-08:00"
    assert close_time(1200, 1000, "2019-11-11T00:00-08:00") == "2019-11-14T03:00:00-08:00"


def test_float_km_acp_times():
    assert open_time(10.25, 200, "2019-11-11T00:00-08:00") == "2019-11-11T00:18:00-08:00"
    assert open_time(10.5, 200, "2019-11-11T00:00-08:00") == "2019-11-11T00:18:00-08:00"
    assert open_time(10.75, 200, "2019-11-11T00:00-08:00") == "2019-11-11T00:19:00-08:00"
